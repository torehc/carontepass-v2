# carontepass-v2

Blog: https://carontepass.wordpress.com/
Documentación: http://wiki.kreitek.org/proyectos:control_acceso

Extra: A partir de Noviembre 2015 los cambios realizados serán valorados para el Consurso Universitario de Software Libre.

## Open Access Control 

El objetivo es crear una aplicación que permite implementar un **sistema de acceso a un 
área física común compartida** por una serie de socios o colaboradores, como por ejemplo,
un HackSpace.
